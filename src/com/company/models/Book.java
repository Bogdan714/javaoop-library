package com.company.models;

import com.company.additional.Writings;

public class Book extends Writings {
    private String author;
    private String publisher;

    public Book(String title, String author, String publisher, int year) {
        super(title, year);
        this.author = author;
        this.publisher = publisher;
    }

    @Override
    public void print() {
        System.out.println(author);
        System.out.println("publisher: " + publisher);
        super.print();
    }


}
