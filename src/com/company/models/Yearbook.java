package com.company.models;

import com.company.additional.Writings;

public class Yearbook extends Writings {
    private String subject;
    private String publisher;

    public Yearbook(String title, String subject, String publisher, int year) {
        super(title, year);
        this.subject = subject;
        this.publisher = publisher;
    }

    @Override
    public void print() {
        System.out.println("publisher: " + publisher + ", #" + subject);
        super.print();
    }
}
