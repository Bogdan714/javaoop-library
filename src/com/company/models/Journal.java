package com.company.models;

import com.company.additional.Writings;

public class Journal extends Writings {
    private String subject;
    private String mounth;

    public Journal(String title, String subject, int year, String month) {
        super(title, year);
        this.subject = subject;
        this.mounth = month;
    }

    @Override
    public void print() {
        System.out.println(mounth + ", #" + subject);
        super.print();
    }
}
