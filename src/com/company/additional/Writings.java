package com.company.additional;

public class Writings {
    private String title;
    private int year;

    public Writings(String title, int year) {
        this.title = title;
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void print() {
        System.out.println("\"" + title + "\" (" + year + ")");
        System.out.println("_____");
    }
}
