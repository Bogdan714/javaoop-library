package com.company.additional;

import com.company.models.Book;
import com.company.models.Journal;
import com.company.models.Yearbook;

public class Library {
    Writings[] writings;

    public Library(int books, int journals, int yearbooks) {
        this.writings = Generator.generateWritings(books, journals, yearbooks);
    }

    public void printByYear(int year) {
        boolean found = false;
        for (Writings writing : writings) {
            if (writing.getYear() == year) {
                writing.print();
                found = true;
            }
        }
        if (!found) {
            System.out.println("Materials not found");
        }
    }
}
