package com.company.additional;

import com.company.models.*;

public class Generator {

    public static Book[] generateBooks(int number) {
        Book[] books = new Book[number];
        for (int i = 0; i < books.length; i++) {
            books[i] = new Book("Book " + i, "Author " + i, "publishing house " + i, 2000 + i);
        }
        return books;
    }

    public static Journal[] generateJournals(int number) {
        Journal[] journals = new Journal[number];
        for (int i = 0; i < journals.length; i++) {
            journals[i] = new Journal("Journal " + i, "News " + i, 2000 + i, "Jun");
        }
        return journals;
    }

    public static Yearbook[] generateYearbooks(int number) {
        Yearbook[] yearbooks = new Yearbook[number];
        for (int i = 0; i < yearbooks.length; i++) {
            yearbooks[i] = new Yearbook("Yearbook " + i, "News", "publishing house " + i, 2000 + i);
        }
        return yearbooks;
    }

    public static Writings[] generateWritings(int booksVal, int journalsVal, int yearbooksVal) {
        Writings[] writings = new Writings[booksVal + journalsVal + yearbooksVal];
        Book[] books = Generator.generateBooks(booksVal);
        Yearbook[] yearbooks = Generator.generateYearbooks(yearbooksVal);
        Journal[] journals = Generator.generateJournals(journalsVal);
        for (int i = 0; i < booksVal; i++) {
            writings[i] = books[i];
        }
        for (int i = booksVal; i < yearbooksVal + booksVal; i++) {
            writings[i] = yearbooks[i - booksVal];
        }
        for (int i = yearbooksVal + booksVal; i < journalsVal + booksVal + yearbooksVal; i++) {
            writings[i] = journals[i - yearbooksVal - booksVal];
        }
        return writings;
    }
}
