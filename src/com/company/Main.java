package com.company;

import com.company.additional.Library;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //init scanner
        Scanner scan = new Scanner(System.in);
        //init library
        Library library = new Library(18, 18, 18);
        System.out.println("Materials for which year(2000 - 2017) you want to see?");
        //input year
        int year = Integer.parseInt(scan.nextLine());
        System.out.println("Materials for " + year + ":");
        System.out.println("____________________");
        //result
        library.printByYear(year);
    }
}